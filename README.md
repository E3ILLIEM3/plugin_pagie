## ตัวอย่าง Pagie.

![](https://gitlab.com/E3ILLIEM3/plugin_pagie/uploads/a02293cd3c66c3153bd5689d2c75241a/image.png)


> ผลลัพธ์ นำ `$_GET['page']` ไปใช้


> 
> ![](https://gitlab.com/E3ILLIEM3/plugin_pagie/uploads/2dc6a9d2c07f72f7a2affbdb76c0d4a1/image.png)
>
---

## วิธีใช้
**ไฟล์ import**
```html
<link rel="stylesheet" href="css/pagie.css">
<script src="js/pagie.js"></script>
```
> **ใช้ `Jquery` อย่าลืม import นะครับ**
---
## Code. ตัวอย่าง
> **คัดลอก Code ด้านล่าง** | ใช้กับไฟล์ .php

```php
<?php
//ตัวอย่างการเอา ผลลัพธ์ นำ $_GET['page'] ไปใช้

                if ($_GET['page'] < 1) {
                header("location:campaigns_suggest?page=1");
                exit();
                }
                
                
                $oop = new \App\Http\Controllers\Controller();
                $sumdata = $oop->xxx(); // ดึงจำนวน Row

                //ตั้งค่าก่อนดึง
                $set_limit = 2;  //จำนวนที่ให้โชว์ในแต่ละเพจ
                $set_ofset = ($_GET['page'] - 1) * $set_limit; //หาค่า index เริ่มต้นของข้อมูล
                
                $value_aaa = $oop->aaa($set_limit, $set_ofset); //เอาไป (SELECT * FROM `xxx` LIMIT `'$set_limit` OFFSET `$set_ofset`) 
                
                //แล้วเอา $value_aaa ไปวน Loop foreach เพื่อแสดงข้อมูล
?>
        
```

```html

<!--UI Pagination-->

<div class="wrap-pagination">
                <?php
                $sum_page = ceil($sumdata / $set_limit); //หาจำนวนหน้าทั้งหมด
                if ($_GET['page'] > $sum_page) {
                header("location:campaigns_suggest?page=" . $sum_page);
                exit();
                }
                ?>

                <div class="wrap-pagie-start" onclick="window.location.href='/campaigns_suggest?page=1'">
                    หน้าแรก
                </div>

                <div class="wrap-previous" style="cursor:pointer"
                     onclick="window.location.href='/campaigns_suggest?page=<?=$_GET['page']-1?>'">
                    ย้อนกลับ
                </div>


                <div class="wrap-content-pagie">

                    <?php
                    if (!isset($_GET['page'])) {
                        header("location:campaigns_suggest?page=1");
                        exit();
                    }
                    else {
                        if ($_GET['page'] == '') {
                            header("location:campaigns_suggest?page=1");
                            exit();
                        }
                    }
                    if ($_GET['page'] > $sum_page) {
                        header("location:campaigns_suggest?page=" . $sum_page);
                        exit();
                    } elseif ($_GET['page'] < 1) {
                        header("location:campaigns_suggest?page=1");
                        exit();
                    }
                    ?>
                    <?php
                    foreach ($sum_page < 5 ? range(1, $sum_page) : ($_GET['page'] + 4 > $sum_page ? range($sum_page - 4, $sum_page) : range($_GET['page'], $_GET['page'] + 4))  AS $as_range){
                        ?>
                    <span style="margin: 2px" sp_pagenum="<?=$as_range?>"
                          onclick="window.location.href='/campaigns_suggest?page=<?=$as_range?>'">
                        <?= $as_range ?>
                    </span>
                        <?php
                    }
                    ?>
                    <?php

                    if ($sum_page <= 5) {

                    }else{
                    if ($_GET['page'] + 5 <= $sum_page){
                        ?>
                    <span style="margin: 2px">
                                        ....
                            </span>
                        <?php
                    }
                    }
                    ?>
                </div>

                <div class="wrap-next" style="cursor:pointer"
                     onclick="window.location.href='/campaigns_suggest?page=<?=$_GET['page']+1?>'">
                    ถัดไป
                </div>
                <div class="wrap-pagie-end" onclick="window.location.href='/campaigns_suggest?page=<?=$sum_page?>'">
                    หน้าสุดท้าย
                </div>
            </div>
```